#!/bin/bash

./openstack_cleanup.sh


./openstack_networking.sh

cat > /etc/hostname <<EOF
$HOST_NAME
EOF

cat > /etc/hosts <<EOF
127.0.0.1     localhost
127.0.1.1     $HOST_NAME

## Add other host manully!

## IPV6
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters

EOF

## set network with interface.conf
cat > /etc/network/interfaces <<EOF
# loopback
auto lo
iface lo inet loopback
iface lo inet6 loopback

# primary interface
auto $DEV_NAME
iface $DEV_NAME inet static
  address $IP
  netmask 255.255.255.0
  gateway $GATEWAY
  dns-nameservers 8.8.8.8

# ipv6 configuration
iface $DEV_NAME inet6 auto
EOF


## restart networking
/etc/init.d/networking restart

## test
./openstack_server_test.sh

## system update
./openstack_system_update.sh

## setup
./openstack_setup.sh

## mysql
./openstack_mysql.sh

## keyston
./openstack_keystone.sh
. ./stackrc;
keystone user-list;


## setup glance
./openstack_glance.sh;

glance image-list;

## Cinder setup
./openstack_cinder.sh;
./openstack_loop.sh;
cinder type-list;

## a test
cinder create --volume-type Storage --display-name test 1

## Nova
./openstack_nova.sh
nova service-list
nova image-list

## Add network
nova-manage network create private --fixed_range_v4=10.0.47.0/24 --num_networks=1 --bridge=br100 --bridge_interface=eth0 --network_size=255;

nova-manage floating create --pool=nova --ip_range=192.168.56.128/25;

nova network-list

## Horizon
./openstack_horizon.sh

cp setuprc /home/xipin/.comp.conf

echo "FINISHED: install controller, you'd better have a reboot!"
