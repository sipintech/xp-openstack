#!/bin/bash

#******************************************************************/
#
#       COMPUTER_SH
#
#       @brief
#
#       @author   Yukang Chen  @date  2014-08-14 14:13:41
#
# ******************************************************************/

./openstack_networking.sh

cat > /etc/hostname <<EOF
$HOST_NAME
EOF

cat > /etc/hosts <<EOF
127.0.0.1     localhost
127.0.1.1     $HOST_NAME

## Add other host manully!

## IPV6
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters

EOF

cat > /etc/network/interfaces <<EOF
# loopback
auto lo
iface lo inet loopback
iface lo inet6 loopback

# primary interface
auto $DEV_NAME
iface $DEV_NAME inet static
  address $IP
  netmask 255.255.255.0
  gateway $GATEWAY
  dns-nameservers 8.8.8.8

# ipv6 configuration
iface $DEV_NAME inet6 auto
EOF


/etc/init.d/networking restart

./openstack_server_test.sh

./openstack_system_update.sh

./openstack_setup.sh

./openstack_cinder.sh

./openstack_loop.sh

./openstack_nova_compute.sh


nova-manage service list

echo "FINISHED: install compute node"
echo "You'd better fix /etc/hosts (including controller rig's hosts)"
echo "and have a reboot :) "
