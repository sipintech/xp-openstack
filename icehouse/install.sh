#!/bin/bash
#******************************************************************/
#
#       INSTALL_CONTROLLER_SH
#
#       @brief
#
#       @author   Yukang Chen  @date  2014-08-14 14:07:07
#
# ******************************************************************/

export RIGIP=192.168.200.100
export GATEWAY=192.168.200.1
export DEV_NAME=eth0
export HOST_NAME=`hostname`

export EMAIL=moorekang@gmail.com
export REGION_NAME=dev-region
export PASSWORD=openstack

## Only for compute node,
## should oget from controller rig, filename is setuprc
export CONTROL_IP=192.168.200.102
export COMP_CONF=".comp.conf"

install_controller() {
    export INSTALL_TYPE=CONTROLLER
    rm -rf ./stackrc
    rm -rf ./setuprc
    ./controller.sh
}

install_compute() {
    scp xipin@$CONTROL_IP:/home/xipin/.comp.conf ./
    if [ -f $COMP_CONF ]
    then
        rm -rf ./stackrc
        rm -rf ./setuprc
        ./computer.sh
    else
        echo "You need set comp_conf according to controller rig !!"
        exit 1;
    fi
}

show_help() {
    echo "-ctrl      : install controller"
    echo "-comp      : install compute node "
    echo "-help|-h   : show help message"
}

while [ $# -gt 0 ]
do
    case $1 in
            -help|-h) show_help; exit 0;;
            -ctrl)    install_controller; exit 0;;
            -comp)    install_compute; exit 0;;
    esac
    shift
done

show_help;
