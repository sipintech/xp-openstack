#!/bin/sh

#******************************************************************/
#
#       OPENSTACK_INSTALL_SH
#
#       @brief
#
#       @author   Yukang Chen  @date  2014-08-18 19:17:23
#
# ******************************************************************/


# install packages for cinder
apt-get install -y lvm2
apt-get install -y python-cinderclient
apt-get install -y cinder-api cinder-scheduler cinder-volume
